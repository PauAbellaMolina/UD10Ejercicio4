package views;

import javax.swing.JOptionPane;
import dto.Calculos;
import exceptions.CustomException;

public class MainView {
	public static double pedirOpcionMenu() throws NumberFormatException, CustomException  {
		//Pedimos al usuario una opcion del menu
		String operacion = JOptionPane.showInputDialog(null, "1) Suma  2) Resta  3) Multiplicacion  4) Potencia  5) Raiz cuadrada  6) Raiz cubica  7) Divison");
		String int1 = "";
		String int2 = "";
		
		do {
			//Switch que segun la opcion del menu introducida llama al metodo correspondiente y en el caso de las raices y la division, lanza la excepcion custom con el codigo correspondiente
			switch (Integer.parseInt(operacion)) {
				case 1:
					int1 = JOptionPane.showInputDialog(null, "Primer numero");
					int2 = JOptionPane.showInputDialog(null, "Segundo numero");
					return Calculos.suma(Double.parseDouble(int1), Double.parseDouble(int2));
				case 2:
					int1 = JOptionPane.showInputDialog(null, "Primer numero");
					int2 = JOptionPane.showInputDialog(null, "Segundo numero");
					return Calculos.resta(Double.parseDouble(int1), Double.parseDouble(int2));
				case 3:
					int1 = JOptionPane.showInputDialog(null, "Primer numero");
					int2 = JOptionPane.showInputDialog(null, "Segundo numero");
					return Calculos.multiplicacion(Double.parseDouble(int1), Double.parseDouble(int2));
				case 4:
					int1 = JOptionPane.showInputDialog(null, "Primer numero");
					int2 = JOptionPane.showInputDialog(null, "Segundo numero");
					return Calculos.potencia(Double.parseDouble(int1), Double.parseDouble(int2));
				case 5:
					int1 = JOptionPane.showInputDialog(null, "Numero");
					if (Double.parseDouble(int1) < 0) {
						throw new CustomException(1);
					} else {
						return Calculos.raizCuadrada(Double.parseDouble(int1));
					}
				case 6:
					int1 = JOptionPane.showInputDialog(null, "Numero");
					if (Double.parseDouble(int1) < 0) {
						throw new CustomException(2);
					} else {
						return Calculos.raizCubica(Double.parseDouble(int1));
					}
				case 7:
					int1 = JOptionPane.showInputDialog(null, "Primer numero");
					int2 = JOptionPane.showInputDialog(null, "Segundo numero");
					if (Double.parseDouble(int1) == 0) {
						throw new CustomException(3);
					} else if (Double.parseDouble(int2) == 0) {
						throw new CustomException(3);
					} else {
						return Calculos.division(Double.parseDouble(int1), Double.parseDouble(int2));						
					}
			}
		}while(!operacion.contentEquals("1") && !operacion.contentEquals("2") && !operacion.contentEquals("3") && !operacion.contentEquals("4") && !operacion.contentEquals("5") && !operacion.contentEquals("6") && !operacion.contentEquals("7"));
		
		return 0;
	}
	
	//Metodo que muestra el resultado recibido
	public static void mostrarResultado(double resultado) {
		System.out.println("Resultado: " + resultado);
	}
}
