package dto;

public class Calculos {
	//Metodos que devuelven los resultados de las operaciones de cada metodo
	public static double suma(double int1, double int2) {
		return int1 + int2;
	}
	
	public static double resta(double int1, double int2) {
		return int1 - int2;
	}
	
	public static double multiplicacion(double int1, double int2) {
		return int1 * int2;
	}
	
	public static double potencia(double int1, double int2) {
		return Math.pow(int1, int2);
	}
	
	public static double raizCuadrada(double int1) {
		return Math.sqrt(int1);
	}
	
	public static double raizCubica(double int1) {
		return Math.cbrt(int1);
	}
	
	public static double division(double int1, double int2) {
		return int1 / int2;
	}
}
