package exceptions;

import views.MainView;

public class CustomException extends Exception {
	public static void mainExceptions() {
		//Pedimos al usuario una opcion de menu y recogemos las excepciones
		try {
			MainView.mostrarResultado(MainView.pedirOpcionMenu());
		}catch(NumberFormatException e) {
			System.out.println("Solo puedes introducir numeros");
		}catch(CustomException ce) {
			System.out.println(ce);
		}
	}
	
	//Atributos
	protected String message;
	
	//Constructor por defecto
	public CustomException() {
		this.message="";
	}
	
	//Constructor que segun el codigo recibido cambia el mensaje de la excepcion
	public CustomException(int codigo) {
		switch (codigo) {
			case 1:
				this.message = "No se puede introducir un numero negativo en una raiz cuadrada";
				break;
			case 2:
				this.message = "No se puede introducir un numero negativo en una raiz cubica";
				break;
			case 3:
				this.message = "No se puede introducir 0 en una division";
		}
	}

	//Metodo toString que devuelve el atributo mensaje
	public String toString() {
		return message;
	}
}
